/* @ngInject */
export default function HomeService ($http, localStorageService, $q) {
  const exports = {
    getItems,
    addItem,
    isPending: false,
    timeoutTest: 10000
  };

  return exports;

  ///////////////

  function getItems(id){
    if(id){
      localStorageService.get(lsKeys[i]);
    }
    const lsKeys = localStorageService.keys();
    const listItems = {};
    for(let i=0; i < lsKeys.length; i++) {
      listItems[lsKeys[i]] = localStorageService.get(lsKeys[i]);
    }
    return listItems;
  }

  function addItem (itemObj){
    const id = (new Date()).getTime();
    if(localStorageService.isSupported && !this.isPending) {
      let defer = $q.defer();
      this.isPending = true;
      setTimeout(function(){
        this.isPending = false;
        if(localStorageService.set(id, itemObj)) {
          defer.resolve(id);
        } else {
          defer.reject("ERROR")
        }        
      }, this.timeoutTest);
      return defer.promise;
    }else {
      return false;
    }
  }
}
