import './add-form.less';
import template from './add-form.html';

export let AddFormComponent = {
    templateUrl: template,
    selector: 'addForm',
    require: {
      parentCtrl: '^home'
    },
    controller: class AddFormCtrl {
      constructor() {
      }
      item = {}
      onSubmit(event){
          console.log("item", this.item);
          event.preventDefault();
          this.parentCtrl.addItem(Object.assign({}, this.item));
          this.item = {};
      }
    }
  };