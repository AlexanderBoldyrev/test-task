import './home.less';
import template from './home.html';

export let HomeComponent = {
  templateUrl: template,
  selector: 'home',
  controller: class HomeCtrl {
    constructor(HomeService) {
      console.log("HomeService", HomeService);
      Object.assign(this, { HomeService });
    }
    listItems = {}
    errorMessage = ""
    selectedItem = {
      id: "",
      description: "Please select item"
    }
    $onInit = () => {
      this.listItems = this.HomeService.getItems();
      console.log("listItems", this.listItems);
    }
    addItem(itemObj){
      
      let addItemPromise = this.HomeService.addItem(itemObj);
      console.log("addItemPromise", addItemPromise);
      if(addItemPromise){
        addItemPromise.then((id) => {
          this.listItems[id] = itemObj;
          this.errorMessage = "";
        }).catch((e) => {
          this.errorMessage = e;
        })
      }else{
        this.errorMessage = "Maybe server is busy now"
      }
    }
    onSelectItem(key, value){
      this.selectedItem.id = key;
      this.selectedItem.description = value.itemDescription
    }
    
  }
};
